import { get } from 'svelte/store'
import { state, disableButtons } from '../stores/store.js'

const clamp = (number, min, max) => {
	return Math.max(min, Math.min(number, max))
}

export const decrease = (store) => {
	store.update((v) => clamp(v - 60, 1 * 60, 60 * 60))
}

export const increase = (store) => {
	store.update((v) => clamp(v + 60, 1 * 60, 60 * 60))
}

export const toTitleCase = (str) => {
	return str.replace(/\w\S*/g, (txt) => {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
	})
}
