import { writable, get } from 'svelte/store'
export let breakTime = writable(60 * 5)
export let sessionTime = writable(60 * 25)
export let state = writable('rest')
export let disableButtons = writable(false)

state.subscribe((v) => {
	console.log()
	disableButtons.set(['work', 'break'].includes(v))
})
