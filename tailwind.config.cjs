const config = {
	mode: 'jit',
	purge: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			colors: {
				'theme-black-0': '#1a2639',
				'theme-black-1': '#303a52',
			},
		},
	},
	plugins: [
		require('tailwind-nord'),
		require('tailwind-dracula')('dracula'),
		require('daisyui'),
	],
}

module.exports = config
